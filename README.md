# coopcycle-jh

## Repo qui va stocker le fichier .jh du projet CoopCycle

Le fichier : **[coopcyle.jh](./coopcycle.jh)**

Le fichier d'initilisation de l'application (non-fonctionel) **[appcoopcyle.jh](appcoopcycle.jh)**

Pour l'importer : **`wget https://gitlab.com/coopcycle_lithaud_alexandre/coopcycle-jh/-/raw/main/coopcycle.jh`**

La commande : **`jhipster import-jdl coopcycle.jh`**
